package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
