package com.tsc.jarinchekhina.tm.controller;

import com.tsc.jarinchekhina.tm.api.controller.ITaskController;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks) System.out.println(task);
        System.out.println("[END LIST TASKS]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[SUCCESS CLEAR TASKS]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[SUCCESS CREATE TASK]");
    }

}
