package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.model.Command;

public interface ICommandService {

    Command[] getConstCommands();

}
