package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.constant.ArgumentConst;
import com.tsc.jarinchekhina.tm.constant.TerminalConst;
import com.tsc.jarinchekhina.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "display developer info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "display program version"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "display list of possible actions"
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "display system information"
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "display list of commands"
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "display list of arguments"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "close application"
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "create new task"
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "remove all tasks"
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "show task list"
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "create new project"
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "remove all projects"
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "show project list"
    );

    public static final Command[] CONST_COMMANDS = new Command[]{
            ABOUT, VERSION, HELP, INFO, ARGUMENTS, COMMANDS, EXIT,
            TASK_CREATE, TASK_CLEAR, TASK_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST
    };

    public Command[] getConstCommands() {
        return CONST_COMMANDS;
    }

}
